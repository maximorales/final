**Pasos para el despliegue de la aplicación 'usuarios'.**

La app web 'usuarios' permite consultar el contenido de una base de datos de usuarios y el registro de nuevas entradas.

_REQUERIMIENTOS PREVIOS: DEBE CONTARSE CON DOS SERVIDORES CON DOCKER Y KUBERNETES INSTALADOS RESPECTIVAMENTE. AMBOS DEBEN ESTAR EN EL MISMO SEGMENTO DE RED._

**1- Construcción de la imagen de docker.**

Utilizar los ficheros contenidos en el directorio 'docker-image'. En una máquina con docker instalado, realizar la construcción de la imagen ejecutando el siguiente comando dentro del mismo directorio local donde se encuentran los archivos. Utilizar el nombre 'usuarios' para la imagen.

$ docker build -t usuarios .


**2- Exportación de la imagen.**

Exportar la imagen a un archivo de extensión '.tar' que luego será utilizado para el despliegue con kubernetes.

$ docker save usuarios > usuarios.tar


**3- Copia del archivo.**

Copiar el archivo generado a una máquina con una instalación de kubernetes. Para eso, ejecutar el siguiente comando para realizar la copia hacia una máquina remota que debe encontrarse en el mismo segmento de red que la máquina con docker y de la cual se conocen las credenciales de acceso.

$ scp usuarios.tar <USER_NAME>@<IP_REMOTE_HOST>:<WORKING_DIRECTORY>

Cuando solicite, colocar la contraseña del usuario.


**4- Importación de la imagen.**

En la máquina con kubernetes, dentro del directorio de trabajo, ejecutar el siguiente comando para realizar la importación de la imagen que luego utilizará el deployment.

$ microk8s ctr image import usuarios.tar

Cuando el proceso termine, deberá visualizar '...done' en la salida del comando.

**5- Despliegue de la app.**

Copiar dentro del directorio de trabajo el fichero 'usuarios-dp.yaml' contenido en la carpeta 'kube-deploy' de este proyecto. Ejecutar el deployment de pods con el siguiente comando.

$ kubectl apply -f usuarios-dp.yaml

Para verificar la ejecución del deployment, utilizar el comando a continuación.

$ kubectl get deployments

En información que brinda la salida del comando deberá observarse un deployment en ejecución con el nombre 'usuarios'.

Utilizando el comando 'get' es posible verificar la existencia de los pods del deployment.

$ kubectl get pods

En la información que brinda la salida del comando podrá visualizarse la réplica del pod generada por el deployment (el número de réplicas que se desea generar puede modificarse cambiando el campo correspondiente dentro del manifiesto yaml).


**6- Redireccionamiento de puertos.**

Utilizar el siguiente comando de creación de proxy para redireccionar puertos al deployment para acceso externo.

$ kubectl port-forward deployment/usuarios 8080:80 --address 0.0.0.0


**7- Acceso a la interfaz web.**

Acceder a la aplicación desde un navegador.
http://<HOST_IP_ADDRESS>:8080


**8- Consulta a la base de datos.**

Para realizar una primera consulta a la base de usuarios utilizar las credenciales de prueba detalladas a continuación.

USERNAME: jperez

CONTRASEÑA: abcd1234

Verificar la correcta conexión a la base visualizando los datos del usuario.


**9- Registro de nuevo usuario.**

Registrar un nuevo usuario desde el botón 'Registrar' y llenando los campos solicitados. Si la registración fue exitosa, se mostrará un mensaje de entrada guardada correctamente.


**10- Consulta de nuevo registro.**

Verificar que la nueva entrada se guardó exitosamente volviendo a ingresar con el USERNAME y la CONTRASEÑA generados en el paso anterior. Se podrán visualizar los datos del nuevo usuario cargado.







